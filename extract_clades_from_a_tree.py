"""
extract a clade using two seq ids on the other side of the trifurcate
"""

import phylo3,newick3,os,sys
import tree_utils


def extract_clade(root,out1,out2,in1):
	#make sure that outgroups exists
	labels = tree_utils.get_front_labels(root)
	if out1 not in labels or out2 not in labels or in1 not in labels:
		print "At least one of the names not found"
		sys.exit()
	max_score,direction,max_node = 0,"",None
	for node in root.iternodes():
		if node == root: continue
		scoreDICT = {} #key is the node object, value is the scores [front,back]
		front,back = 0,0
		front_labels = tree_utils.get_front_labels(node)
		if out1 not in front_labels and out2 not in front_labels and in1 in front_labels:
			front = len(front_labels)
		else: front = -1
		back_labels = tree_utils.get_back_labels(node,root)
		if out1 not in back_labels and out2 not in back_labels and in1 in back_labels:
			back = len(back_labels)
		else: back = -1
		scoreDICT[node] = [front,back]
		if front > max_score:
			max_score,direction,max_node = front,"front",node
			print max_score,direction
		if back > max_score:
			max_score,direction,max_node = back,"back",node
			print max_score,direction
	if direction == "front":
		return max_node
	elif direction == "back":
		par = max_node.parent
		par.remove_child(max_node)
		max_node.prune()
		return phylo3.reroot(root,par)#flip dirction
	
if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python extract_clades_from_tree.py intree seqID1_to_exclude seqID2_to_exclude seq_to_include"
		sys.exit(0)
	
	input_tree = sys.argv[1]
	out1 = sys.argv[2]
	out2 = sys.argv[3]
	in1 = sys.argv[4]
	with open(input_tree,"r") as infile:
		 curroot = newick3.parse(infile.readline())#one tree in each file
	clade = extract_clade(curroot,out1,out2,in1)
	taxa = set(tree_utils.get_front_names(clade))
	print len(taxa),"front taxa found"
	for i in taxa: print i
	with open(input_tree.split(".")[0]+"ext.tre","w") as outfile:
		outfile.write(newick3.tostring(clade)+";\n")
			
