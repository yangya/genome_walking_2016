"""
Input: a starting fasta, and the itinitial baits
Output: refined tree(s) and correcponding fasta file(s)
"""

import os,sys, newick3,phylo3
import tree_utils
import cut_long_internal_branches
import seq

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python "+sys.argv[0]+" all_fasta beet_clusters_merged indir outdir long_branch_cutoff"
		sys.exit(0)
	
	all_fasta,beet_clusters_merged,indir,outdir,long_branch_cutoff = sys.argv[1:]
	indir += "/"
	outdir += "/"
	
	# read in cluster id and focal beet loci
	print "Reading in the beet_clusters_merged"
	count = 0
	infile = open(beet_clusters_merged,"r")
	homologDICT = {} # key is homolog id, value is a list of beta seqids
	for line in infile:
		spls = line.strip().split("\t")
		if len(spls) == 0: continue
		count += 1
		homologDICT[str(count)] = spls
	infile.close()
	print len(homologDICT),"clusters read"
	
	# cut input tree and write out subtree and fasta for the next round
	# that contains the focal beet loci
	for i in os.listdir(indir):
		if not i.endswith(".mm"): continue
		print i
		print set(homologDICT[homologid])
		with open(indir+i,"r") as infile:
			intree = newick3.parse(infile.readline())
		subtrees = cut_long_internal_branches.cut_long_internal_branches(intree,cutoff=float(long_branch_cutoff))
		count = 0
		base_name = i.split(".")[0]
		homologid = (base_name.split("_")[0]).replace("homolog","")
		seqDICT = {} # key is seqid, value is seq
		for tree in subtrees:
			if tree != None: 
				label_set = set(tree_utils.get_front_labels(tree))
				if len(label_set) >= 4 and len(label_set & set(homologDICT[homologid])) > 0:
					count += 1
					if tree.nchildren == 2: #fix bifurcating roots from cutting
						temp,tree = tree_utils.remove_kink(tree,tree)
					with open(outdir+base_name+"_"+str(count)+".subtree","w") as outfile:
						outfile.write(newick3.tostring(tree)+";\n")
		print count, "subtree(s) written"

	print "Reading in all fasta seqs"
	seqDICT = {} # key is seqid, value is seq
	for s in seq.read_fasta_file(all_fasta):
		seqDICT[s.name] = s.seq
	print len(seqDICT),"sequences read"
	for i in os.listdir(outdir):
		if not i.endswith(".subtree"): continue
		with open(outdir+i,"r") as infile:
			intree = newick3.parse(infile.readline())
		with open(outdir+i.replace(".subtree",".fa"),"w") as outfile:
			for seqid in tree_utils.get_front_labels(intree):
				try:
					outfile.write(">"+seqid+"\n"+seqDICT[seqid]+"\n")
				except:
					print seqid,"not found in fasta file"
	
