This repository contains scripts for homology inference using the modified phylome approach from our 218 taxa Caryophyllales data set.

Citation: Yang, Y., M.J. Moore, S.F. Brockington, J. Mikenas, J. Olivieri, J.F. Walker, and S.A. Smith. 2017. Improved transcriptome sampling pinpoints 26 paleopolyploidy events in Caryophyllales, including two paleo-allopolyploidy events. Accepted. New Phytologist. 

Choose a taxonID for each data set. This taxonID will be used throughout the analysis. Use short taxonIDs with 4-6 letters and digits with no special characters. Avoid special characters except "_" for seq ids. See our dryad data package for examples of input sequence format. 

Code in this repository was developed based on https://bitbucket.org/yangya/adh_2016 to accomodate much larger data sets.

File formats are hard coded and code may not give the most informative error message if your data has formatting issues.

###Prepare backbone homologs
Concatenate peptide sequences from all 43 genomes into a fasta file named "43genomes.pep.fa".

	cat *.pep.fa >43genomes.pep.fa

Make a blast database using this combined peptide fasta file:

	makeblastdb -in 43genomes.pep.fa -parse_seqids -dbtype prot -out 43genomes.pep.fa

For each beet locus, combine splice variants into one single fasta file.

	python spls_baits.py Beta.pep.fa <outdir>

To build a phylome for each beet locus, the following pipeline was used to do the SWIPE search, parse and filtering results, and build and trim the putative homolog tree.

	python bait_homologs_genome.py <query_pep_fa_dir> 43genomes.pep.fa num_cores outdir

This creates refined homolog trees for each beet locus that end with .subtree, which are the phylomes. Copy these refined homolog trees into a new dir and merge homologs according to these refined trees

	python merge_genome_clusters.py <phylome dir>

This output a file named "beet_clusters_merged", with each line having beet loci that were duplicated within the Caryophyllales and should be merged into a single homolog. 


###Sort transcriptome data according to the backbone homologs
Run cd-hit on each peptide data set translated from assembled transcriptomes
For example, for the peptide data set AAXJ.pep.fa

	cd-hit -i AAXJ.pep.fa -o AAXJ.cdhit.pep.fa -c 0.99 -n 5 -T 4

SWIPE each peptide sequence from the transcriptomes against the beet proteome with an E-value cutoff of 0.01. The top hit for each query peptide was recorded in a dir called "swipe_output". Also, concatenate all peptide files from genomes and transcriptomes into 218taxa.pep.fa

	python sort_homologs_genome_walking.py <dir containing *.subtree> beet_clusters_merged swipe_output_dir <path to 218taxa.pep.fa> outdir

This will output homologs with transcripts added, and a file named beet_clusters_merged. I moved all homologs larger than 5000 to a separate dir and ignore them for now. 

###Refine homologs
A tree was constructed for each resulting fasta file with the script 

	python fasta_to_tree.py <DIR> <number_cores> aa n

Spurious tips in the resulting trees are trimmed

	python trim_tips.py <tree dir> .tre 1 2

Monophyletic and paraphyletic tips that belong to the same taxa were reduced

	python mask_tips_by_taxonID_transcripts.py <dir with trimmed trees end with .tt> <dir with trimmed alignments end with -cln> y

Cut long internal branches, and save the subtree that contains the original beet locus

	python refine_homolog_genome_walking.py <combined pep fasta file for all taxa> beet_clusters_merged <input dir> <output dir> <branch length cutoff>

This process is repeated once or twice. Final trimming settings (more stringent):

	python trim_tips.py <tree dir> .tre 0.5 1
	python cut_long_internal_branches.py <inDIR> .tt 0.5 4 <outdir> 
 

###Orthology inference
Scripts in this step can be found in bitbucket.org/yangya/phylogenomic_dataset_construction
	
	python prune_paralogs_RT.py <homoTreeDIR> <homotree_file_eneding> <outDIR> <min_ingroup_taxa> taxon_code_file

Write ortholog fasta files

	python write_ortholog_fasta_files.py 218taxa_pep.fa OrthoTreeDIR outDIR 160

Align by PRANK

	python prank_wrapper.py inDIR outDIR .fa aa

Trim alignments using Phyutility

	python phyutility_wrapper.py DIR .3 aa

Trim tips by relative cutoff of 0.3 and absolute cutoff of 0.6

	python ~/genome_walking_src_2016/trim_tips.py DIR .tre 0.3 0.6

Remove internal branches longer than 0.5

	python cut_long_internal_branches.py inDIR .tt 0.5 160 outDIR

Write alignments from trimmed trees, phyutility by 0.3 using similar procedures as above. Bootstrap

	python raxml_bs_wrapper.py DIR 4 aa




