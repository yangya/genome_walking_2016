"""
merge baits for genome walking from filtered swipe output

swipe output colums are:
Query id, Subject id, % identity, alignment length, mismatches, 
gap openings, q. start, q. end, s. start, s. end, e-value, bit score
"""

import sys,os
import seq
import networkx as nx

def get_geneid(seqid):
	"""seq looks like Beta@Bv2_029870_niqq_t1"""
	print seqid
	geneid = seqid.split("_")[2]
	assert len(geneid) == 4, "check seqid format"
	return geneid
	
if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python merge_baits.py swipe_out fasta outdir"
		sys.exit(0)
	
	swipe_out,fasta,outdir = sys.argv[1:]
	
	seqDICT = {} # key is geneid, value is output fasta string
	for s in seq.read_fasta_file(fasta):
		seqid,seq = s.name,s.seq
		geneid = get_geneid(seqid)
		if geneid not in seqDICT:
			seqDICT[geneid] = ""
		seqDICT[geneid] += ">"+seqid+"\n"+seq+"\n"
		
	infile = open(swipe_out,"r")
	G = nx.Graph() # initiate the graph
	for line in infile:
		if len(line) < 3: continue
		spls = line.strip().split("\t")
		if float(spls[-1]) < 50.0:
			continue # remove short seqs and disconnect weak links
		query_geneid, hit_geneid = get_geneid(spls[0]), get_geneid(spls[1])
		print query_geneid, hit_geneid
		G.add_edge(query_geneid,hit_geneid)
	infile.close()

	ccs = nx.connected_components(G) # get the connected components
	for cc in ccs:
		with open(outdir+"/"+cc[0]+".pep.fa","w") as outfile:
			for geneid in cc:
				outfile.write(seqDICT[geneid])
	



