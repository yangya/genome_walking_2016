"""
Input:

- prepare a input fasta file that contains sequences used as bait:
	genename.pep.fa

- prepare a directory with candidate sequence pep fasta files named
	taxonid.pep.fa, or
	taxonid.pep.fa.cdhitent
  for each sequence, the sequence ids look like taxonid@seqid

swipe, taking the top 20 output, construct the homolog
and carry out two rounds of refinement
replace the seqids to long id for visualization in figtree
"""

import phylo3,newick3,os,sys
from swipe_dir import swipe
import fasta_to_tree
import ntpath
import refine_homolog
import taxon_name_subst

# do not mask tips for these genomes, 43 in total
GENOMES = ["Achn","Acoerulea","Athaliana","Atrichopoda","Beta",\
		   "Brapa","Bstricta","Caan","Cclementina","Cila",\
		   "Coca","Cpapaya","Crubella","Csativus","Csinensis",\
		   "Dica","Egrandis","Elgu","Esalsugineum","Fvesca",\
		   "Gmax","Graimondii","Lusitatissimum","Mesculenta","Mguttatus",\
		   "Mtruncatula","Musa","Nenu","Osativa","Pheq",\
		   "Phoe","Ppersica","Ptrichocarpa","Rara","Rcommunis",\
		   "Slycopersicum","Spolyrhiza","Spurpurea","Tcacao","Utri.plus",\
		   "Vvinifera","Zmays","Spol"]

def get_filename_from_path(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python bait_homologs.py query_pep_fa pepDIR num_to_bait num_cores outdir"
		sys.exit(0)
	
	query_fasta,pepdir,num_to_bait,num_cores,out = sys.argv[1:]
	# the query fasta name is gene_name.pep.fa
	gene_name = get_filename_from_path(query_fasta).split(".")[0]
						
	"""
	get the initial fasta files
	wipe query_fasta against each data set in DIR
	write output in outdir
	take the top num_to_bait hits ranked by bitscore
	evalue set to 10
	"""
	outdir = out+"/"+gene_name+"/"
	try: os.stat(outdir)
	except: os.mkdir(outdir)
	if not os.path.exists(outdir+gene_name+".swipe.fa"):
		swipe(query_fasta=query_fasta,\
			pepdir=pepdir,\
			outdir=outdir,\
			num_cores=num_cores,\
			max_num_hits=num_to_bait)
	
	# first round of refine
	refined_fastas = refine_homolog.refine(query_fasta=query_fasta,\
		start_fasta=outdir+gene_name+".swipe.fa",\
		deep_paralog_cutoff=1.0,
		num_cores=num_cores)
	print refined_fastas
	
	# second round of refine
	new_fastas = []
	if len(refined_fastas) > 0:
		for i in refined_fastas:
			new_fastas += refine_homolog.refine(query_fasta=query_fasta,\
				start_fasta=i,\
				deep_paralog_cutoff=0.5,
				num_cores=num_cores)
	print new_fastas
	refined_fastas = new_fastas
				
	# if last round was using fastree, refine one more time
	if len(refined_fastas) > 0 and os.path.exists(outdir+gene_name+"_1.fasttree.tre"):
		for i in refined_fastas:
			refine_homolog.refine(query_fasta=query_fasta,\
				start_fasta=i,\
				deep_paralog_cutoff=0.5,
				num_cores=num_cores)
	
	# change the names for all the .mm and .subtree files for visualization
	for i in os.listdir(outdir):
		if i.endswith(".mm") or i.endswith(".subtree"):
			taxon_name_subst.taxon_name_subst(original=outdir+i)
			
